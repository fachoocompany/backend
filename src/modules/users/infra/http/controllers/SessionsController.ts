import { Request, Response } from "express";
import { container } from "tsyringe";

import { userTransformer } from "@modules/users/utils/userTransformer";
import AuthenticateUserService from "../../../services/AuthenticateUserService";

class SessionsController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { email, password } = request.body;

    const authenticateUserService = container.resolve(AuthenticateUserService);

    const { user, token } = await authenticateUserService.execute({
      email,
      password,
    });

    return response.json({ user: userTransformer(user), token });
  }
}

export default SessionsController;
